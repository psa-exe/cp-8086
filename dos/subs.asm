; Codigos de syscalls
SYS_FOPEN  equ 3D00h
SYS_FCLOSE equ 3E00h
SYS_FREAD  equ 3F00h
SYS_FWRITE equ 4000h
SYS_EXIT   equ 4C00h

; Handles de arquivos padrao
STDIN  equ 0
STDOUT equ 1

; Modos de abertura de arquivo
MODE_R   equ 0h
MODE_W   equ 1h
MODE_RW  equ 2h

; Outras constantes
FILE_LEN equ 09h
BUFF_LEN equ 20h

; Codigos ASCII uteis
LF equ 0Ah
CR equ 0Dh

Subst segment

	assume cs:Subst

	Entrada:
		; Fazer 'es = ds = cs'
		push	cs
		pop		ds
		push	cs
		pop		es

		; Inicializar tabela de traducao
		call IniTab

		; Pedir o nome do arquivo na entrada padrao
		mov		ax, SYS_FWRITE
		mov		bx, STDOUT
		mov		cx, len_a
		lea		dx, msg_a
		int		21h

		; Ler o nome do arquivo da entrada padrao
		mov		ax, SYS_FREAD
		mov		bx, STDIN
		mov		cx, FILE_LEN
		lea		dx, file_name
		int		21h

		; Remover CRLF e adicionar '\0' no final do nome
		sub		ax,  2
		mov		di, dx
		add		di, ax
		mov		al, 00h
		stosb

		; Abrir arquivo para leitura
		mov		ax, SYS_FOPEN
		int		21h

		; Manter o filehandle em 'bp' e 'dx' apontando pro buffer
		mov		bp, ax
		lea		dx, buffer
		jnc   LerLinha

		; Arquivo inexistente
		mov		ax, SYS_FWRITE
		mov		bx, STDOUT
		mov		cx, len_b
		lea		dx, msg_b
		int		21h

		; Programa termina em erro
		mov		ax, SYS_EXIT + 01h
		int		21h

	; Le uma linha do arquivo por vez e
	; realiza as substituicoes necessarias
	LerLinha:
		mov		ax, SYS_FREAD
		mov		bx, bp
		mov		cx, BUFF_LEN
		int		21h

		; Final de arquivo alcancado?
		cmp		ax, 0
		jz		Fechar

		; Copia comprimento da linha para 'cx' e
		; para a pilha e prepara a tabela de traducao
		push	ax
		lea		bx, tabela
		mov		cx, ax

		; Prepara registradores de índice
		cld
		mov		si, dx
		mov		di, dx

	; A cada etapa, carrega um byte, o traduz
	; e o armazena na mesma posicao
	Traduzir:
		lodsb
		xlat
		stosb
		loop	Traduzir

		; Escrever linha
		mov		ax, SYS_FWRITE
		mov		bx, STDOUT
		pop		cx
		int 	21h
		jmp		LerLinha

	Fechar:
		; Fecha arquivo
		mov		ax, SYS_FCLOSE
		mov		bx, ax
		int		21h

	Saida:
		; Encerrar o programa normalmente
		mov		ax, 4C00h
		int		21h


	IniTab proc near
		std
		mov		al, 127
		lea		si, tabela
		lea		di, tabela[127]
	GravarUm:
		stosb
		dec		al
		cmp		di, si
		jae		GravarUm

		mov		al, '.'
		lea		di, tabela['a']
		stosb
		lea		di, tabela['e']
		stosb
		lea		di, tabela['i']
		stosb
		lea		di, tabela['o']
		stosb
		lea		di, tabela['u']
		stosb

		mov		al, '!'
		lea		di, tabela['A']
		stosb
		lea		di, tabela['E']
		stosb
		lea		di, tabela['I']
		stosb
		lea		di, tabela['O']
		stosb
		lea		di, tabela['U']
		stosb

		ret

	IniTab endp

	; Mensagens do programa e seus tamanhos
	msg_a db "Entre com o nome do arquivo (maximo de 9 caracteres)",CR,LF
	len_a equ $-msg_a
	msg_b db "Arquivo especificado nao existe",CR,LF
	len_b equ $-msg_b

	; Variaveis diversasas
	file_name db (FILE_LEN + 2) dup(0)
	buffer db (BUFF_LEN + 1) dup(?)
	tabela db 128 dup(?)

Subst ends

Pilha segment stack
	espaco db 100 dup (0)
Pilha ends

	end Entrada
