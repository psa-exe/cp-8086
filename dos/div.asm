SYS_FREAD  equ 3F00h
SYS_FWRITE equ 4000h
SYS_EXIT   equ 4C00h

STDIN  equ 0
STDOUT equ 1

Divisivel segment
	assume cs:Divisivel

	msgDiv db "Eh divisivel",10,13
	tamDiv equ $-msgDiv
	msgNaoDiv db "Nao eh divisivel",10,13
	tamNaoDiv equ $-msgNaoDiv

	Inicio:
		push	ss
		pop		ds

		call LeNum
		push ax
		call LeNum
		mov	 di, ax
		pop  ax
		xor  dx, dx
		div  di
		or   dx, dx
		jz   Divisivel
		mov		ax, SYS_FWRITE
		mov		bx, STDOUT
		mov		cx, tamNaoDiv
		lea		dx, msgNaoDiv
		int		21h
		jmp		Saida

	Divisivel:
		mov		ax, SYS_FWRITE
		mov		bx, STDOUT
		mov		cx, tamDiv
		lea		dx, msgDiv
		int		21h

	Saida:
		; Sair do programa
		mov		ax, SYS_EXIT
		int 	21h

	LeNum proc
			xor		di, di
		ProxCarac:
			mov		ax, SYS_FREAD
			mov		bx, STDIN
			mov		cx, 1
			sub		sp, 2
			mov		dx, sp
			int		21h
			pop		cx
			sub		cl, '0'
			cmp		cl, 10
			jnc		FimNum
			mov		ax, 10
			mul		di
			xor		ch, ch
			add		ax, cx
			mov		di, ax
			jmp		ProxCarac
		FimNum:
		 mov ax, di
			ret

	LeNum endp

Divisivel ends

Pilha segment stack
	Reserva dw 100 dup(?)
Pilha ends

	end Inicio
