SYS_FOPEN  equ 3D00h
SYS_FCLOSE equ 3E00h
SYS_FWRITE equ 4000h
SYS_EXIT   equ 4C00h

STDIN  equ 0
STDOUT equ 1

Dados segment
	assume ds:Dados
		arquivo db "arq.txt",0
		msg_erro db "Arquivo inexistente",10,13
		len_erro equ $-msg_erro
Dados ends

Quinta segment
	assume cs:Quinta

	Inicio:
		;push	cs
		;pop		ds

		mov		ax, SYS_FOPEN
		lea		dx, ds:arquivo
		int		21h
		mov		bp, ax
		jnc		Continuar

		; Escrever mensagem de erro
		mov		ax, SYS_FWRITE
		mov		bx, STDOUT
		mov		cx, ds:len_erro
		lea		dx, ds:msg_erro
		int		21h

		; Encerrar com erro
		mov		ax, SYS_EXIT + 01h
		int		21h

	Continuar:
		mov		ax, SYS_FCLOSE
		mov		bx, bp
		int		21h

		; Sair do programa
		mov		ax, SYS_EXIT
		int 	21h

Quinta ends

Pilha segment stack
	Reserva dw 100 dup(?)
Pilha ends

	end Inicio
